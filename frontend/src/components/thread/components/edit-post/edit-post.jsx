import * as React from 'react';
import PropTypes from 'prop-types';
import { IconName } from 'src/common/enums/enums';
import { Button, Form, Image, Segment, Icon } from 'src/components/common/common';

import styles from './styles.module.scss';

const EditPost = ({ post, updatePostImage, uploadImage, cancelPostUpdate, updatePost }) => {
  const [body, setBody] = React.useState(post.body);
  const [image, setImage] = React.useState(post.image);
  const [isUploading, setIsUploading] = React.useState(false);

  const handleChangeImage = async ({ target }) => {
    setIsUploading(true);
    const postImage = target.files[0];

    uploadImage(postImage)
      .then(({ id: imageId, link: imageLink }) => {
        updatePostImage(post.id, postImage);
        setImage({ imageId, imageLink });
      })
      .catch(() => {
      })
      .finally(() => {
        setIsUploading(false);
      });
  };

  const handleRemoveImage = () => {
    updatePostImage(post.id, undefined);
    setImage(undefined);
  };

  const handleUndoPostUpdate = () => {
    cancelPostUpdate(post);
  };

  const handleEditPost = () => {
    updatePost(post.id, { body, image });
  };

  return (
    <Segment>
      <Form.TextArea
        name="body"
        value={body}
        placeholder="Update post"
        onChange={ev => setBody(ev.target.value)}
      />
      {image?.imageLink && (
        <div className={styles.imageWrapper}>
          <Image className={styles.image} src={image?.imageLink} alt="post" />
        </div>
      )}
      <div className={styles.btnWrapper}>
        <Button
          color="teal"
          isLoading={isUploading}
          isDisabled={isUploading}
          iconName={IconName.IMAGE}
        >
          <label className={styles.btnImgLabel}>
            Change image
            <input
              name="image"
              type="file"
              onChange={handleChangeImage}
              hidden
            />
          </label>
        </Button>
        <Button
          color="red"
          icon
          labelPosition="left"
          as="label"
          onClick={handleRemoveImage}
        >
          <Icon name="trash" />
          Remove image
        </Button>
      </div>
      <div className={styles.buttonsContainerRight}>
        <Button color="blue" icon labelPosition="left" as="label" onClick={handleUndoPostUpdate}>
          <Icon name="remove" />
          Cancel
        </Button>
        <Button color="blue" icon labelPosition="left" as="label" onClick={handleEditPost}>
          <Icon name="check" />
          Update
        </Button>
      </div>
    </Segment>
  );
};

EditPost.propTypes = {
  post: PropTypes.isRequired,
  updatePostImage: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  cancelPostUpdate: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired
};

export default EditPost;
