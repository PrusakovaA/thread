import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label } from 'src/components/common/common';
import EditPost from 'src/components/thread/components/edit-post/edit-post';

import styles from './styles.module.scss';

const Post = ({ post, isEdit, inEdit, editPost, updatePost, updatePostImage,
  cancelPostUpdate, uploadImage, onPostLike, onPostDislike, onExpandedPostToggle, sharePost }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    updatedAt
  } = post;
  const date = getFromNowTime(createdAt);
  const update = updatedAt;

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handleEditPost = () => editPost(id);

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className={isEdit ? 'date-edit-post' : 'date'}>
            {update ? 'update by' : 'posted by'}
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
          {isEdit && (
            <Label
              basic
              size="small"
              as="a"
              className={styles.toolbarBtn}
              onClick={handleEditPost}
            >
              <Icon name="edit" />
            </Label>
          )}
        </Card.Meta>
        <Card.Description>
          {inEdit
            ? (
              <EditPost
                post={post}
                uploadImage={uploadImage}
                updatePost={updatePost}
                updatePostImage={updatePostImage}
                cancelPostUpdate={cancelPostUpdate}
              />
            )
            : (body)}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {likeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={handlePostDislike}>
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  isEdit: PropTypes.bool.isRequired,
  inEdit: PropTypes.bool.isRequired,
  editPost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  updatePostImage: PropTypes.func.isRequired,
  cancelPostUpdate: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default Post;
